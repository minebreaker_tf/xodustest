package sample;

import jetbrains.exodus.ByteIterable;
import jetbrains.exodus.bindings.StringBinding;
import jetbrains.exodus.entitystore.*;
import jetbrains.exodus.env.Environment;
import jetbrains.exodus.env.Environments;
import jetbrains.exodus.env.Store;
import jetbrains.exodus.env.StoreConfig;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public final class Sample {

    Path cwd = Paths.get("./xodus.database");

    @Test
    public void test() {

        Environment env = Environments.newInstance(cwd.toFile());
        env.clear();

        Store store = env.computeInTransaction(
                txn -> env.openStore("Store", StoreConfig.WITHOUT_DUPLICATES, txn));

        ByteIterable key = StringBinding.stringToEntry("kye");
        ByteIterable value = StringBinding.stringToEntry("value");

        ByteIterable res1 = env.computeInReadonlyTransaction(
                txn -> store.get(txn, key));

        assertThat(res1, is(nullValue()));

        env.executeInTransaction(
                txn -> store.put(txn, key, value));
        ByteIterable res2 = env.computeInReadonlyTransaction(
                txn -> store.get(txn, key));

        assertThat(StringBinding.entryToString(res2), is("value"));

        env.clear();
        env.close();
    }

    @Test
    public void testEntityStore() {
        PersistentEntityStore entityStore = PersistentEntityStores.newInstance(cwd.toFile());
        entityStore.clear();

        long size = entityStore.computeInTransaction(txn -> {
            EntityIterable iter = txn.getAll("entity");
            return iter.size();
        });
        assertThat(size, is(0L));

        EntityId id = entityStore.computeInTransaction(txn -> {
            Entity entity = txn.newEntity("entity");
            entity.setProperty("prop1", "abc");
            entity.setProperty("prop2", "123");
            return entity.getId();
        });

        String res = entityStore.computeInTransaction(txn -> {
            Entity e = txn.getEntity(id);
            String prop1 = (String) e.getProperty("prop1");
            String prop2 = (String) e.getProperty("prop2");
            return prop1 + prop2;
        });
        assertThat(res, is("abc123"));

        entityStore.clear();
        entityStore.close();

    }

    @Test
    public void testSort() {
        PersistentEntityStore entityStore = PersistentEntityStores.newInstance(cwd.toFile());
        entityStore.executeInTransaction(txn -> {
            Entity entity = txn.newEntity("entity");
            entity.setProperty("prop1", "abc");
            entity.setProperty("prop2", "123");
            Entity entity2 = txn.newEntity("entity");
            entity.setProperty("prop1", "def");
            entity.setProperty("prop2", "456");
        });

        @SuppressWarnings("ConstantConditions")
        String prop1 = entityStore.computeInTransaction(
                txn -> (String) txn.sort("entity", "prop1", true).getFirst().getProperty("prop2"));

        assertThat(prop1, is("abc"));

        @SuppressWarnings("ConstantConditions")
        String prop2 = entityStore.computeInTransaction(
                txn -> (String) txn.sort("entity", "prop2", false).getFirst().getProperty("prop1"));

        assertThat(prop2, is("def"));

        entityStore.clear();
        entityStore.close();
    }

}
